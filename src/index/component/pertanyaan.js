import React, { Component } from 'react'
import { observer, inject } from "mobx-react"
import Tests from '../../test-data';
import Table from 'react-bulma-components/lib/components/table';
import Heading from 'react-bulma-components/lib/components/heading';
import Section from 'react-bulma-components/lib/components/section';
import Level from 'react-bulma-components/lib/components/level';
import Box from 'react-bulma-components/lib/components/box';
import Button from 'react-bulma-components/lib/components/button'

let Unit = observer((props) => {
    return <tr>
        <td>{props.data.ask}</td>
        <td className="has-text-centered">
            <input disabled={props.disabled} type="radio" name={props.id} value="a" onChange={(event)=>{
                props.data.value=event.target.value;
            }}/>
        </td>
        <td className="has-text-centered">
            <input disabled={props.disabled} type="radio" name={props.id} value="a" onChange={(event)=>{
                props.data.value=event.target.value;
            }}/>
        </td>
        <td className="has-text-centered">
            <input disabled={props.disabled} type="radio" name={props.id} value="d" onChange={(event)=>{
                props.data.value=event.target.value;
            }}/>
        </td>
        <td className="has-text-centered">
            <input disabled={props.disabled} type="radio" name={props.id} value="d" onChange={(event)=>{
                props.data.value=event.target.value;
            }}/>
        </td>
    </tr>
})

@inject('questionStore')
@observer
export default class Pertanyaan extends Component {
    componentWillMount = () => {
      this.props.questionStore.questions = Tests;
    }
    
    render() {
        const {questions,  revealAnswer, score, allowedToReveal} = this.props.questionStore;
        return (
            <div>
                <Section>
                <Table>
                    <thead>
                        <tr>
                            <th>Description</th>
                            <th className="has-text-centered">Definitely agree</th>
                            <th className="has-text-centered">Slightly agree</th>
                            <th className="has-text-centered">Slightly disagree</th>
                            <th className="has-text-centered">Definitely  disagree</th>
                        </tr>
                    </thead>
                    <tbody>
                        {questions.map((data,i) => {
                            return <Unit 
                                data={data} 
                                key={'tes-' + i} 
                                id={'tes-' + i} 
                                disabled={revealAnswer}
                            />;
                        })}
                    </tbody>
                </Table>
                &copy; MRC-SBC/SJW February 1998. Published: Journal of Autism and Developmental Disorders, 31, 5-17 (2001).
                </Section>
                <hr />
                <Section>
                    <Heading> And Your Test Result are...</Heading>
                    {revealAnswer?
                        <div>
                            <Box>
                                <Level renderAs="nav">
                                    <Level.Item style={{textAlign: "center"}}>
                                        <div>
                                            <Heading renderAs="p" heading>
                                                Score
                                            </Heading>
                                            <Heading renderAs="p">
                                                {score}
                                            </Heading>
                                        </div>
                                    </Level.Item>
                                    <Level.Item style={{textAlign: "center"}}>
                                        <div>
                                            <Heading renderAs="p" heading>
                                                Result
                                            </Heading>
                                            <Heading renderAs="p">
                                                {score>=32?"posibly-autistic":"might-not-autistic"}
                                            </Heading>
                                        </div>
                                    </Level.Item>
                                </Level>
                            </Box>
                            <p>
                                80% of people who got scored 32 and above are diagnosed with autism or a related disorder scored 32 or higher.<br/>
                                You might need to do a test with doctor.
                            </p>
                            <p>
                                Oh, fun fact, i got mine 32, and i think i'm not that autistic.
                            </p>
                        </div>:
                        <div>
                            {!allowedToReveal?<p className="has-text-danger">
                                You have to fill all those forms first before we can calculate your result.
                            </p>:""}
                            <p>
                                If you're sure, click this button to reveal your result. <br/>
                                Please note that this is <b>NOT</b> in any meaning to diagnose you.
                            </p>
                            <p>
                                Once you click the button, the form will be locked, and you have to refresh the page.
                            </p>
                            <Section>
                                <Button color="info" onClick={()=>{this.props.questionStore.revealAnswer=true}} disabled={!allowedToReveal}>
                                    Reveal Answer
                                </Button>
                            </Section>
                        </div>
                    }
                </Section>
            </div>
        )
    }
}
