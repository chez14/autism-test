import React, { Component } from 'react'
import Content from 'react-bulma-components/lib/components/content'
import Container from 'react-bulma-components/lib/components/container'
import Footer from 'react-bulma-components/lib/components/footer'
import  { FontAwesomeIcon as FA} from '@fortawesome/react-fontawesome';
export default class FooterT extends Component {
    render() {
        return (
            <Footer>
                <Container>
                    <Content style={{ textAlign: "center" }}>
                        <p>
                            Made <FA icon="love"/> by <b>Chez14</b> with React.js, Bulma, and Mobx.
                        </p>
                    </Content>
                </Container>
            </Footer>
        )
    }
}
