import {observable, computed} from 'mobx'

export default class QuestionStore {
    @observable 
        questions = [{}];
    @observable
        revealAnswer = false;
    
    @computed
        get score() {
            let score = 0;
            this.questions.forEach(tes => {
                if(tes.value === tes.zone)
                    score++;
            });
            return score;
        }
    
    @computed 
        get allowedToReveal() {
            return (this.questions.filter((a)=>a.value == null)).length == 0;
        }
}