import React, { Component } from 'react';
import Columns from 'react-bulma-components/lib/components/columns';
import Notification from 'react-bulma-components/lib/components/notification';
import Hero from 'react-bulma-components/lib/components/hero';
import Container from 'react-bulma-components/lib/components/container';
import Heading from 'react-bulma-components/lib/components/heading';
import Button from 'react-bulma-components/lib/components/button';
import Section from 'react-bulma-components/lib/components/section';
import Pertanyaan from './component/pertanyaan';
import {observer, Provider } from "mobx-react"
import QuestionStore from './component/question-store';
import Footer from './component/footer';

class App extends Component {
  render() {
    return (
      <div>
        <Hero color="black">
          <Hero.Body>
            <Container>
              <Section>
                <Heading>
                  Autism Quick Test
                </Heading>
                <Heading subtitle size={5}>
                  Please fill up these form as the result will be calculated down below.
                </Heading>
              </Section>
            </Container>
          </Hero.Body>
        </Hero>
        <Container>
          <Columns>
            <Columns.Column desktop={{size:10}} tablet={{size:12}}>
              <Section>
                A little note, this test was made by Psychologist Simon Baron-Cohen and his colleagues at Cambridge’s Autism Research Centre,
                the test was taken from <a href="https://www.wired.com/2001/12/aqtest/">WIRED's Take the Autism Test page</a>. Altough it has form
                that fillable, the calculation are still must be done manually as they stated that the code is too old and being renewed, but it felt
                like too long, so i made this page just to make you satisfied.<br />
                <b>No data are sent from your browser, i don't track the visitors (for now), and i never collect any data submitted by you (in meaning of
                filling this form).</b>
                <Notification color="warning">
                  Please note that <strong>The test is not a means for making a diagnosis.</strong> I'm a college student,
                  and i'm not the creator of this test and so i don't have any rights to make diagnosis whether you're
                  autistic or not.
                </Notification>
              </Section>
            </Columns.Column>
          </Columns>
          <Columns>
            <Columns.Column desktop={{size:10}} tablet={{size:12}}>
              <Provider questionStore={new QuestionStore()}>
                <Pertanyaan />
              </Provider>
            </Columns.Column>
          </Columns>
        </Container>
        <Footer/>
      </div>
    );
  }
}

export default App;
