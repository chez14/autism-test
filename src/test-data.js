// Zone: a: Agree, d: disagree
export default [
    {
        "ask": "I prefer to do things with others rather than on my own.",
        "zone": "d" // 1
    },
    {
        "ask": "I prefer to do things the same way over and over again.",
        "zone": "a"
    },
    {
        "ask": "If I try to imagine something, I find it very easy to create a picture in my mind.",
        "zone": "d"
    },
    {
        "ask": "I frequently get so strongly absorbed in one thing that I lose sight of other things.",
        "zone": "a"
    },
    {
        "ask": "I often notice small sounds when others do not.",
        "zone": "a" // 5
    },
    {
        "ask": "I usually notice car number plates or similar strings of information.",
        "zone": "a"
    },
    {
        "ask": "Other people frequently tell me that what I’ve said is impolite, even though I think it is polite.",
        "zone": "a"
    },
    {
        "ask": "When I’m reading a story, I can easily imagine what the characters might look like.",
        "zone": "d"
    },
    {
        "ask": "I am fascinated by dates.",
        "zone": "a"
    },
    {
        "ask": "In a social group, I can easily keep track of several different people’s conversations.",
        "zone": "d" // 10
    },
    {
        "ask": "I find social situations easy.",
        "zone": "d"
    },
    {
        "ask": "I tend to notice details that others do not.",
        "zone": "a"
    },
    {
        "ask": "I would rather go to a library than to a party.",
        "zone": "a"
    },
    {
        "ask": "I find making up stories easy.",
        "zone": "d"
    },
    {
        "ask": "I find myself drawn more strongly to people than to things.",
        "zone": "d" // 15
    },
    {
        "ask": "I tend to have very strong interests, which I get upset about if I can’t pursue.",
        "zone": "a"
    },
    {
        "ask": "I enjoy social chitchat.",
        "zone": "d"
    },
    {
        "ask": "When I talk, it isn’t always easy for others to get a word in edgewise.",
        "zone": "a"
    },
    {
        "ask": "I am fascinated by numbers.",
        "zone": "a"
    },
    {
        "ask": "When I’m reading a story, I find it difficult to work out the characters’ intentions.",
        "zone": "a" // 20
    },
    {
        "ask": "I don’t particularly enjoy reading fiction.",
        "zone": "a"
    },
    {
        "ask": "I find it hard to make new friends.",
        "zone": "a"
    },
    {
        "ask": "I notice patterns in things all the time.",
        "zone": "a"
    },
    {
        "ask": "I would rather go to the theater than to a museum.",
        "zone": "d"
    },
    {
        "ask": "It does not upset me if my daily routine is disturbed.",
        "zone": "d" // 25
    },
    {
        "ask": "I frequently find that I don’t know how to keep a conversation going.",
        "zone": "a"
    },
    {
        "ask": "I find it easy to ‘read between the lines’ when someone is talking to me.",
        "zone": "d"
    },
    {
        "ask": "I usually concentrate more on the whole picture, rather than on the small details.",
        "zone": "d"
    },
    {
        "ask": "I am not very good at remembering phone numbers.",
        "zone": "d"
    },
    {
        "ask": "I don’t usually notice small changes in a situation or a person’s appearance.",
        "zone": "d" // 30
    },
    {
        "ask": "I know how to tell if someone listening to me is getting bored.",
        "zone": "d"
    },
    {
        "ask": "I find it easy to do more than one thing at once.",
        "zone": "d"
    },
    {
        "ask": "When I talk on the phone, I’m not sure when it’s my turn to speak.",
        "zone": "a"
    },
    {
        "ask": "I enjoy doing things spontaneously.",
        "zone": "d"
    },
    {
        "ask": "I am often the last to understand the point of a joke",
        "zone": "a" // 35
    },
    {
        "ask": "I find it easy to work out what someone is thinking or feeling just by looking at their face.",
        "zone": "d"
    },
    {
        "ask": "If there is an interruption, I can switch back to what I was doing very quickly.",
        "zone": "d"
    },
    {
        "ask": "I am good at social chitchat.",
        "zone": "a"
    },
    {
        "ask": "People often tell me that I keep going on and on about the same thing.",
        "zone": "a"
    },
    {
        "ask": "When I was young, I used to enjoy playing games involving pretending with other children.",
        "zone": "d" // 40
    },
    {
        "ask": "I like to collect information about categories of things (e.g., types of cars, birds, trains, plants).",
        "zone": "a"
    },
    {
        "ask": "I find it difficult to imagine what it would be like to be someone else.",
        "zone": "a"
    },
    {
        "ask": "I like to carefully plan any activities I participate in.",
        "zone": "a"
    },
    {
        "ask": "I enjoy social occasions.",
        "zone": "d"
    },
    {
        "ask": "I find it difficult to work out people’s intentions.",
        "zone": "a" // 45
    },
    {
        "ask": "New situations make me anxious.",
        "zone": "a"
    },
    {
        "ask": "I enjoy meeting new people.",
        "zone": "d"
    },
    {
        "ask": "I am a good diplomat.",
        "zone": "d"
    },
    {
        "ask": "I am not very good at remembering people’s date of birth.",
        "zone": "d"
    },
    {
        "ask": "I find it very easy to play games with children that involve pretending.",
        "zone": "d" //50
    },
]
