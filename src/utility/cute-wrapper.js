import React, { Component } from 'react'
import Navbar from 'react-bulma-components/lib/components/navbar';
import Tag from 'react-bulma-components/lib/components/tag';
import Container from 'react-bulma-components/lib/components/container';
import { Link } from 'react-router-dom'
import  { FontAwesomeIcon as FA} from '@fortawesome/react-fontawesome';

export default class CuteWrapper extends Component {
  render() {
    return (
      <div>
        <Navbar color="black">
            <Container>
                <Navbar.Brand>
                    <Link to="/" className="navbar-item">
                        Autism-test&nbsp;
                        <Tag.Group gapless>
                            <Tag color="info">coded by</Tag>
                            <Tag>chez14</Tag>
                        </Tag.Group>
                    </Link>
                    <Navbar.Burger/>
                </Navbar.Brand>
                <Navbar.Menu>
                    <Navbar.Container position="end">
                        <Navbar.Item href="https://gitlab.com/chez14/autism-test" >
                            <FA icon="code-branch" />
                        </Navbar.Item>
                    </Navbar.Container>
                </Navbar.Menu>
            </Container>
        </Navbar>
        {this.props.children}
      </div>
    )
  }
}
