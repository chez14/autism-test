import React from 'react';
import ReactDOM from 'react-dom';
import App from './index/index';
import './index.sass';
import CuteWrapper from './utility/cute-wrapper';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faCodeBranch } from '@fortawesome/free-solid-svg-icons'

library.add(fab, faCodeBranch);

ReactDOM.render(
    <Router>
        <Switch>
            <Route exact path="/" render={()=><Redirect to="/autism-test"/>}/>
            <CuteWrapper>
                <Route exact path="/autism-test" component={App}/>
            </CuteWrapper>
        </Switch>
    </Router>
, document.getElementById('root'));
